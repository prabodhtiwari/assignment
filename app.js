var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'cars'
});
connection.connect(function(err){
	if(!err) {
		console.log("Database is connected");    
	} else {
		console.log("Error connecting database");    
	}
});
app.use(bodyParser.urlencoded({ extended: true }));
var methodOverride = require('method-override') 
app.use(methodOverride('X-HTTP-Method-Override',['DELETE','UPDATE']))
app.use(function(req, res, next) {
    if (req.headers.origin) {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Headers', 'X-HTTP-Method-Override')
        res.header('Access-Control-Allow-Methods', '*')
        if (req.method === 'OPTIONS') return res.send(200)
    }
    next()
});

app.get('/show', function(req, res, next) {
	connection.query('SELECT * from car_details', function(err, rows, fields) {
		if (!err){
			res.json(rows);
		}else{
			console.log(err);
		}
	});
});
app.delete('/', function(req, res, next) {
	connection.query('DELETE from car_details where id = '+req.body.field, function(err, rows, fields) {
		if (!err){
			res.json({ message: 'Success' });
		}else{
			console.log(err);
		}
	});
});

app.post('/update', function(req, res) {
	connection.query('UPDATE car_details SET make="'+ req.body.make + '" , model="'+ req.body.model + '" , year="'+ req.body.year + '"where id = '+req.body.id, function(err, rows, fields) {
		if (!err){
			res.json({ message: 'Success' });
		}else{
			console.log(err);
		}
	});
});

app.post('/create', function(req, res) {
	connection.query('Insert Into car_details (make, model, year) values("'+ req.body.make + '" , "'+ req.body.model + '" ,"'+ req.body.year + '")', function(err, rows) {
		if (!err){
			res.json({ message: 'Success', result:rows });
		}else{
			console.log(err);
		}
	});  
});

app.post('/search', function(req, res) {
	connection.query('SELECT * from car_details where make LIKE "%'+ req.body.field + '%" or model LIKE   "%'+ req.body.field + '%" or year LIKE "%'+ req.body.field +'%"', function(err, rows) {
		if (!err){
			res.json(rows);
		}else{
			console.log(err);
		}
	});  
});

app.listen(8080, function() {
  console.log('Server running at http://127.0.0.1:8080/');
});