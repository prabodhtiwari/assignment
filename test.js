"use strict";
var Car = (function () {
    function Car(make, model, year) {
        this.make = make;
        this.model = model;
        this.year = year;
    }
    return Car;
}());
function create(car) {
    fetch('http://127.0.0.1:8080/create', {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: 'make=' + car.make + '&model=' + car.model + '&year=' + car.year
    }).then(function (response) {
        return response.text();
    }).then(function (body) {
        var data = JSON.parse(body);
        if (data.message = 'success') {
            document.getElementById('data').innerHTML = document.getElementById('data').innerHTML + "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr id =" + data.result.insertId + ">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + data.result.insertId + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + car.make + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + car.model + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + car.year + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='submit' value='delete' onclick='deleteData(" + data.result.insertId + ")'>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='submit' value='update' onclick=\"updateData(" + data.result.insertId + ", '" + car.make + "', '" + car.model + "', '" + car.year + "')\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>";
            document.getElementById('make').value = "";
            document.getElementById('model').value = "";
            document.getElementById('year').value = "";
        }
        else {
            alert("Some error");
        }
    });
}
function updateData(id, make, model, year) {
    var newcar = new Car(make, model, year);
    update(newcar, id);
}
function update(car, id) {
    document.getElementById(id).innerHTML = "<td>" + id + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='text' id='makedata' value=" + car.make + ">\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='text' id='modeldata' value=" + car.model + ">\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='text' id='yeardata' value=" + car.year + ">\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='submit' value='delete' onclick='deleteData(" + id + ")'>\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='submit' value='save' onclick='saveData(" + id + ", makedata.value,modeldata.value,yeardata.value)'>\n\t\t\t\t\t\t\t\t\t\t\t\t</td>";
}
function saveData(id, make, model, year) {
    var newcar = new Car(make, model, year);
    save(newcar, id);
}
function save(car, id) {
    fetch('http://127.0.0.1:8080/update', {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: 'id=' + id + '&make=' + car.make + '&model=' + car.model + '&year=' + car.year
    }).then(function (response) {
        return response.text();
    }).then(function (body) {
        var data = JSON.parse(body);
        if (data.message = 'success') {
            document.getElementById(id).innerHTML = "<td>" + id + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + car.make + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + car.model + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + car.year + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='submit' value='delete' onclick='deleteData(" + id + ")'>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='submit' value='update' onclick='updateData(" + id + ", '" + car.make + "', '" + car.model + "', '" + car.year + "')'>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>";
        }
        else {
            alert("Some error");
        }
    });
}
function search(str) {
    if (str.length < 1) {
        document.getElementById('data').innerHTML = "";
        indexData();
    }
    else {
        fetch('http://127.0.0.1:8080/search', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: 'field=' + str
        }).then(function (response) {
            return response.text();
        }).then(function (body) {
            var data = JSON.parse(body);
            document.getElementById('data').innerHTML = "";
            if (data.length > 0) {
                data.forEach(function (value) {
                    var searchedcar = new Car(value.make, value.model, value.year);
                    document.getElementById('data').innerHTML = document.getElementById('data').innerHTML + "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr id =" + value.id + ">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + value.id + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + searchedcar.make + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + searchedcar.model + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + searchedcar.year + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='submit' value='delete' onclick='deleteData(" + value.id + ")'>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='submit' value='update' onclick=\"updateData(" + value.id + ", '" + searchedcar.make + "', '" + searchedcar.model + "', '" + searchedcar.year + "')\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>";
                });
            }
            else {
                document.getElementById('data').innerHTML = "No Data Availiable";
            }
        });
    }
}
function deleteData(id) {
    fetch('http://127.0.0.1:8080', {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-HTTP-Method-Override': 'DELETE'
        },
        body: 'field=' + id
    }).then(function (response) {
        return response.text();
    }).then(function (body) {
        var data = JSON.parse(body);
        if (data.message = 'success') {
            document.getElementById(id).innerHTML = "";
        }
        else {
            alert("Some error");
        }
    });
}
function indexData() {
    fetch('http://127.0.0.1:8080/show')
        .then(function (response) {
        return response.text();
    }).then(function (body) {
        var data = JSON.parse(body);
        if (data.length > 0) {
            data.forEach(function (value) {
                var storedcar = new Car(value.make, value.model, value.year);
                document.getElementById('data').innerHTML = document.getElementById('data').innerHTML + "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr id =" + value.id + ">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + value.id + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + storedcar.make + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + storedcar.model + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>" + storedcar.year + "</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='submit' value='delete' onclick='deleteData(" + value.id + ")'>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type='submit' value='update' onclick=\"updateData(" + value.id + ", '" + storedcar.make + "', '" + storedcar.model + "', '" + storedcar.year + "')\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>";
            });
        }
        else {
            document.getElementById('data').innerHTML = "No Data Availiable";
        }
    });
}
function createData(make, model, year) {
    var newcar = new Car(make, model, year);
    create(newcar);
}
