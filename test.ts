"use strict";
declare let fetch: any
class Car {
	constructor(public make, public model, public year) {
	}
}

interface Icar {
	make: string;
    model: string;
    year: string;
}

function create(car : Icar) {
	fetch('http://127.0.0.1:8080/create', {
		method: 'post',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: 'make=' + car.make + '&model=' + car.model + '&year=' + car.year
	}).then(function(response) {
		return response.text()
	}).then(function(body) {
		let data = JSON.parse(body);
		if (data.message = 'success'){
			document.getElementById('data').innerHTML = `${document.getElementById('data').innerHTML}
														<tr id =${ data.result.insertId }>
															<td>${ data.result.insertId }</td>
															<td>${ car.make }</td>
															<td>${ car.model }</td>
															<td>${ car.year }</td>
															<td>
																<input type='submit' value='delete' onclick='deleteData(${ data.result.insertId })'>
															</td>
															<td>
																<input type='submit' value='update' onclick="updateData(${ data.result.insertId }, '${ car.make }', '${ car.model }', '${ car.year }')">
															</td>
														</tr>`;
			(<HTMLInputElement>document.getElementById('make')).value = "";
			(<HTMLInputElement>document.getElementById('model')).value = "";
			(<HTMLInputElement>document.getElementById('year')).value = "";
		}else{
			alert("Some error");
		}
	});
}

function updateData(id, make, model, year) {
	const newcar = new Car(make, model, year);
    update(newcar, id);
}

function update(car : Icar, id){
	document.getElementById(id).innerHTML = `<td>${ id }</td>
												<td>
													<input type='text' id='makedata' value=${ car.make }>
												</td>
												<td>
													<input type='text' id='modeldata' value=${ car.model }>
												</td>
												<td>
													<input type='text' id='yeardata' value=${ car.year }>
												</td>
												<td>
													<input type='submit' value='delete' onclick='deleteData(${ id })'>
												</td>
												<td>
													<input type='submit' value='save' onclick='saveData(${ id }, makedata.value,modeldata.value,yeardata.value)'>
												</td>`;
}

function saveData(id, make, model, year) {
	const newcar = new Car(make, model, year);
    save(newcar, id);
}

function save(car : Icar, id){
	fetch('http://127.0.0.1:8080/update', {
		method: 'post',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: 'id=' + id + '&make=' + car.make + '&model=' + car.model + '&year=' + car.year
	}).then(function(response) {
		return response.text()
	}).then(function(body) {
		let data = JSON.parse(body);
		if (data.message = 'success'){
			document.getElementById(id).innerHTML = `<td>${ id }</td>
														<td>${ car.make }</td>
														<td>${ car.model }</td>
														<td>${ car.year }</td>
														<td>
															<input type='submit' value='delete' onclick='deleteData(${ id })'>
														</td>
														<td>
															<input type='submit' value='update' onclick='updateData(${ id }, '${ car.make }', '${ car.model }', '${ car.year }')'>
														</td>`;
		}else{
			alert("Some error");
		}
    });
}

function search(str) {
	if (str.length < 1){
		document.getElementById('data').innerHTML = "";
		indexData();
	} else{
		fetch('http://127.0.0.1:8080/search', {
			method: 'post',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			body: 'field='+str
		}).then(function(response) {
			return response.text()
		}).then(function(body) {
			let data = JSON.parse(body);
			document.getElementById('data').innerHTML = "";
			if (data.length > 0){
				data.forEach(function (value){
					const searchedcar = new Car(value.make, value.model, value.year);
					document.getElementById('data').innerHTML = `${document.getElementById('data').innerHTML}
																<tr id =${ value.id }>
																	<td>${ value.id }</td>
																	<td>${ searchedcar.make }</td>
																	<td>${ searchedcar.model }</td>
																	<td>${ searchedcar.year }</td>
																	<td>
																		<input type='submit' value='delete' onclick='deleteData(${ value.id })'>
																	</td>
																	<td>
																		<input type='submit' value='update' onclick="updateData(${ value.id }, '${ searchedcar.make }', '${ searchedcar.model }', '${ searchedcar.year }')">
																	</td>
																</tr>`;
				});
			} else{
					document.getElementById('data').innerHTML = "No Data Availiable";
			}
		});
	}
}

function deleteData(id) {
	fetch('http://127.0.0.1:8080', {
		method: 'post',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/x-www-form-urlencoded',
			'X-HTTP-Method-Override': 'DELETE'
		},
		body: 'field='+id
	}).then(function(response) {
		return response.text()
	}).then(function(body) {
		let data = JSON.parse(body);
		if (data.message = 'success'){
			document.getElementById(id).innerHTML = "";
		}else{
			alert("Some error");
		}
	});
}

function indexData() {
	fetch('http://127.0.0.1:8080/show')
	.then(function(response) {
		return response.text()
	}).then(function(body) {
		let data = JSON.parse(body);
		if (data.length > 0){
			data.forEach(function (value){
				let storedcar = new Car(value.make, value.model, value.year);
				document.getElementById('data').innerHTML = `${document.getElementById('data').innerHTML}
															<tr id =${ value.id }>
																<td>${ value.id }</td>
																<td>${ storedcar.make }</td>
																<td>${ storedcar.model }</td>
																<td>${ storedcar.year }</td>
																<td>
																	<input type='submit' value='delete' onclick='deleteData(${ value.id })'>
																</td>
																<td>
																	<input type='submit' value='update' onclick="updateData(${ value.id }, '${ storedcar.make }', '${ storedcar.model }', '${ storedcar.year }')">
																</td>
															</tr>`;
			});
		} else{
				document.getElementById('data').innerHTML = "No Data Availiable";
		}
	});
}

function createData(make, model, year){
	const newcar = new Car(make, model, year);
    create(newcar);
}